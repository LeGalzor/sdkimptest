package com.example.gal.sdkimptest

import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerLib

import kotlinx.android.synthetic.main.activity_main.*
import java.util.HashMap

class MainActivity : AppCompatActivity() {

    internal val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        /* Track Events in real time */
        val eventValue = HashMap<String, Any>()
        eventValue[AFInAppEventParameterName.REVENUE] = 200
        eventValue[AFInAppEventParameterName.CONTENT_TYPE] = "category_a"
        eventValue[AFInAppEventParameterName.CONTENT_ID] = "1234567"
        eventValue[AFInAppEventParameterName.CURRENCY] = "USD"
        AppsFlyerLib.getInstance().trackEvent(applicationContext, AFInAppEventType.PURCHASE, eventValue)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
